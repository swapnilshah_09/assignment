<?php
class Model
{
    private $tableName;
    
    public function __construct($con){
    	$this->con = $con;
		$this->tableName = 'url_mapping';
    }
/*
Function to check whether short key created already exists in db. 
*/
    public function shortKeyExistInDb($key) {

    	$sql = "SELECT short_key FROM ".$this->tableName." WHERE short_key=".$key;

    	$query = mysqli_query($this->con, $sql);
        $row = mysqli_fetch_all($query);
        if(empty($row)) {
            return false;
        } else {
            return true;
        }

					

    }

/*
Function to inser the new long url entered in serch bo on UI and return the list of urls.
*/


    public function insertShortUrl($longUrl,$shortKey) {

            $date = date('Y-m-d H:i:s');
            $sql = "INSERT INTO url_mapping (long_url, short_key, date_created)
                        SELECT * FROM (SELECT '$longUrl', '$shortKey', '$date') AS tmp
                        WHERE NOT EXISTS (
                            SELECT long_url FROM url_mapping WHERE long_url = '$longUrl'
                        ) LIMIT 1;";
            $query = mysqli_query($this->con, $sql);
            
            $getAllData = $this->getListOfUrls();
            if(empty($getAllData)) {
                return;
            } else {
                return $getAllData;
            }
    }


/*
Function to get list of urls that are there in Table with a limit of 100.
*/
    public function getListOfUrls() {
        $sql = "SELECT long_url, short_key FROM ".$this->tableName." limit 100";
        $query = mysqli_query($this->con, $sql);
        $row = mysqli_fetch_all($query);
        if(empty($row)) {
            return;
        } else {
            return $row;
        }        
    }
}
?>


