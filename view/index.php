<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
		<script type="text/javascript" src="includes/common.js"></script>
		
		<link rel="stylesheet" type="text/css" href="includes/style.css">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	    
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	    
	    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
		
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>

	<body>
		<div>
			<div style="text-align: center;" >
					<h1 class="container w-250">
				Url Shortener
			</h1>
			</div>
		
			<div class="container">
      			<div class="justify-content-center">
        			<div class="searchbar">
          				<input class="search_input" type="text" id="long-url" name="long_url" placeholder="Paste a long url">
          				<button onclick="urlShortener()" class="search_icon"><i class="fas fa-search"></i></button>
        			</div>
      				<div class="header">
						<h2 id="list-header" >
					
						</h2>
						<div id="list">
					
						</div>
					</div>
      			</div>
    		
    		</div>
		
		
		</div>
	</body>

</html>