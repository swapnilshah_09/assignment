<?php 
//Ajax controller to inser data in db and get list of urls that are already converted to short urls.
include(dirname(__DIR__).'/includes/config.php');
include(dirname(__DIR__).'/model/Base.php');

if(isset($_POST['long_url']) && !empty($_POST['long_url'])) {
	//get short key that would be used to create short_url
	$short = createShortKey();
	// creating an objet of model. and passing the connection prsent in config.php file.
	$model = new Model($con);

//making db call to model to insert data and return the data from db that is present in table after insertion.

	$existingData = $model->insertShortUrl($_POST['long_url'], $short);
	
	header("Content-type: application/json");

	//encoding th data recieved frommodel to json type.
	
	echo json_encode(array('response' => 'success','data' => $existingData));
} else {
	header("Content-type: application/json");	
	echo json_encode(array('response' => 'fail'));
}

//function to create short unique key and check whether it is used in db already.

function createShortKey() {
	include(dirname(__DIR__).'/includes/config.php');
	$digits = 6;
	$short = rand(pow(10, $digits-1), pow(10, $digits)-1);
	$model = new Model($con);
	//check short exists in db or not.
	$ifKeyExists = $model->shortKeyExistInDb($short);
	if(!$ifKeyExists) {
		return $short;
	} else {
		$this->createShortKey();
	}
}


?>


