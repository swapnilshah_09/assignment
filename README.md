# assignment

--> Clone this project inside you apache htdocs folder

--> please create a database named assignment.

--> please verify the database once more in includes/config.php and other settings required to make db connection in the same file.

--> please execute the db.sql file inside the above created database. 

--> Please open localhost/assignment and you will see the input field where you need to enter the long url to convert it to short url.

--> Once you click on search magnifier on the UI the url you have entered will be inserted to db if it is not already prsent and on UI you will see the list of urls which are there in db after insertion takes place.

--> There is a limit for the number of urls fetched that is 100 urls.
