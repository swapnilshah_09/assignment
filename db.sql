SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `assignment`
--
-- --------------------------------------------------------

--
-- Table structure for table `url_mapping`
--


CREATE TABLE `url_mapping` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `long_url` varchar(255) NOT NULL,
 `short_key` varchar(6) NOT NULL,
 `date_created` datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;