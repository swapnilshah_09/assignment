/*
** function	to make the ajax call to insert the data in db and 
** fetch the list of urls that are converted to short url
*/

function urlShortener() {

	var url = $("#long-url").val();
	
	//ajax call passing the long url that is recieved from input field.
	
	var data = $.ajax({
	    type: "POST",
	    url: "controller/AjaxController.php",
	    async: false,
	    dataType: 'json',
	    data: {
	        long_url: url
	    },
	    success: function (data) {
	    	
	    	if(data.response === 'success') {
			
				$('#list').html('');
		        var header = "List Of most common urls that are coverted to short url";
		        $('#list-header').html(header);
		        		
		        for (var x = 0; x < data.data.length; x++) {

		        	//creating the html from the response recieved from aja controller.

	                var anchor = document.createElement('A');
					anchor.text = "http://shorty.otg/"+data.data[x][1];
					anchor.setAttribute('href', data.data[x][0]);
					var br = document.createElement('BR');

					//appending the anchor tag to the div with id 'list' in view

					document.getElementById('list').appendChild(anchor).appendChild(br);
				}
			} else {
				alert('Please enter a url in the input field.')
			}			
	        
	        return data;
	    }
	});

}